package ru.antosik.charreplacement;

import ru.antosik.charreplacement.Classes.Replacer;

public class Main {

    public static void main(String[] args) {
        String replaced = Replacer.replace("В каждом слове текста k-ю букву заменить заданным символом. Если k больше длины слова, корректировку не выполнять.", 2, 'x');
        System.out.println(replaced);

        Replacer replacer = new Replacer("В каждом слове текста k-ю букву заменить заданным символом. Если k больше длины слова, корректировку не выполнять.");
        String multiReplaced = replacer
                .replace(7, 'x')
                .replace(5, 'y')
                .replace(3, 'z')
                .replace(2, '!')
                .GetString();
        System.out.println(multiReplaced);
    }
}
