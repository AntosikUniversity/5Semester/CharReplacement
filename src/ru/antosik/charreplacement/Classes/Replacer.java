package ru.antosik.charreplacement.Classes;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Replacer {
    /***
     * Replaces every k symbol of words in string with symbol
     * @param string String in which we replace chars
     * @param k Char in word to replace
     * @param symbol Symbol which will be on k index
     * @return Modified string
     */
    static public String replace(String string, int k, char symbol) {
        if (string == null) throw new NullPointerException("String can't be null");
        if (k < 0) {
            throw new IllegalArgumentException("k must be greater, than 0!");
        }

        Pattern pattern = Pattern.compile("\\p{L}+");
        Matcher matcher = pattern.matcher(string);
        while (matcher.find()) {
            String found = matcher.group(0);
            char[] chars = found.toCharArray();
            int stringLength = found.length();

            if (stringLength < k) continue;

            for (int i = k - 1; i < stringLength; i += k) {
                chars[i] = symbol;
            }

            String replacement = new String(chars);
            string = string.replaceFirst(found, replacement);
        }

        return string;
    }

    private String _string;

    /**
     * Constructor
     *
     * @param string String in which we will replace chars
     */
    public Replacer(String string) {
        _string = Objects.requireNonNull(string, "string must not be null");
    }

    /**
     * Replaces every k symbol of words in string with symbol
     *
     * @param k      Char in word to replace
     * @param symbol Symbol which will be on k index
     * @return Modified string
     */
    public Replacer replace(int k, char symbol) {
        _string = replace(_string, k, symbol);
        return this;
    }

    /***
     * Getter for string
     * @return String
     */
    public String GetString() {
        return _string;
    }

}
